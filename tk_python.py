import tkinter as tk
from tkinter import filedialog
from tkinter import *
import os
from os import walk
from os.path import join
from PIL import Image


#_from_Khalil_Khalil_created
#_von_Khalil_Khalil_erstellt
class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.image_folder = tk.Button(self)
        self.image_folder["text"] = "Images Folder"
        self.image_folder["command"] = self.images_path
        self.image_folder.pack(side="top")
		
        self.new_image_save_folder = tk.Button(self)
        self.new_image_save_folder["text"] = "Folder to save new Images"
        self.new_image_save_folder["command"] = self.new_save_images_path
        self.new_image_save_folder.pack(side="top")
		
        self.go = tk.Button(self, text="GO", fg="red", command=self.convert_image)
        self.go.pack(side="bottom")

    def images_path(self):
	    folder_selected = filedialog.askdirectory()
	    self.old_image_folder = folder_selected
        
		
    def new_save_images_path(self):
        folder_selected = filedialog.askdirectory()
        self.new_image_folder = folder_selected
		
    def convert_image(self):
        f = []
        for filenames in walk(self.old_image_folder):
            f.extend(filenames)
        for i in f[2]:
            image_file = Image.open(f[0]+'/'+i)
            image = image_file.convert('L')
            os.chdir(self.new_image_folder)
            image.save('new_'+i)
            os.chdir(self.old_image_folder)
		
		
		
root = tk.Tk()
app = Application(master=root)
app.mainloop()